Message = new Mongo.Collection("Message"); 
if (Meteor.isClient) {
  Template.body.helpers({ 
    messages: function () { 
        var a = Message.find({},{sort: {createdAt : 1}}).fetch().slice(-15);
        return a;//, {sort: {createdAt: -1}} , limit: 20 
    }, 
    Session: function () { 
      return Session.get("Session"); 
    }, 
    Count: function () { 
      return Message.find({}).count(); 
    },
    list_users: function () { 
      var username = _.uniq(
        Message.find({}, {sort: {username: 1}, fields: {username: true}}).fetch().map(function(x) {
        return x.username;
      }), true);
      return username;
    }
  }); 
  Template.body.events({ 
    "submit .new-message": function (event) { 
      if($('#btn-input').val().trim().length==0){
        event.preventDefault(); 
      }else{
        var text = event.target.text.value;
        event.preventDefault(); 
        Meteor.call("addMessage", text);
        event.target.text.value = "";     
      }
    },
    "change .hide-completed input": function (event) { 
      Session.set("Session", event.target.checked); 
    },
    "keypress #btn-input":function(event){
      if(event.ctrlKey && event.keyCode == 13){
        var valor = $('#btn-input').val();
        event.preventDefault;
        valor = valor + "\n";
      }
    }
  }); 
  Template.message.events({ "click .toggle-checked": function () { 
    Meteor.call("setChecked", this._id, ! this.checked); 
    },
    "click .delete": function () { 
      Meteor.call("deleteMessage", this._id); 
    } 
  });
  Template.message.rendered = function () {
    var d = $('#aqui'); 
    d.scrollTop (d[0].scrollHeight - d.height ());
  };
  
  Accounts.ui.config({ 
    passwordSignupFields: "USERNAME_ONLY" 
  }); 
}
Meteor.methods({ addMessage: function (text) {
  if (! Meteor.userId()) { 
    throw new Meteor.Error("not-authorized");
  } 
  Message.insert({ 
    text: text,
    createdAt: new Date(), 
    owner: Meteor.userId(), 
    username: Meteor.user().username }); 
  }, 
  deleteMessage: function (messageId) { 
    Message.remove(messageId); 
  }, 
  setChecked: function (taskId, setChecked) { 
    Message.update(messageId, { $set: { checked: setChecked} }); 
  } 
});